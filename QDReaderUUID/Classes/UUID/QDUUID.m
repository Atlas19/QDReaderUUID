//
//  QDUUID.m
//  QDReaderAtom
//
//  Created by YangYuxin on 16/7/13.
//  Copyright © 2016年 qidian. All rights reserved.
//

#import "QDUUID.h"
#import "KeychainWrapper.h"
#import "NSString+MD5Addition.h"

@implementation QDUUID
+ (NSString *)creatUUID {
    //create a new UUID
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    //get the string representation of the UUID
    NSString* uuidString = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuidObj));
    CFRelease(uuidObj);
    return uuidString;
}

+ (NSString *)currentUUID {
    NSString* uuid = [[NSUserDefaults standardUserDefaults] objectForKey:@"KEY_UUID"];
    KeychainWrapper *keyChainWrapper = [[KeychainWrapper alloc] init];
    if (uuid) {
        [keyChainWrapper mySetObject:uuid forKey:(id)kSecAttrAccount];
        return uuid;
    }
    else {
        uuid = [keyChainWrapper myObjectForKey:(id)kSecAttrAccount];
        if (!uuid || [uuid isEqualToString:@"Account"]) {
            uuid = [QDUUID creatUUID];
            NSLog(@"QDUUID:uuid = %@ created.", uuid);
            [keyChainWrapper mySetObject:uuid forKey:(id)kSecAttrAccount];
        }
        [[NSUserDefaults standardUserDefaults] setValue:uuid forKey:@"KEY_UUID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"QDUUID:uuid = %@ in keycahin", uuid);
        return uuid;
    }
}

+ (NSString *)newDeviceIdentifier{
    
    NSString *udid = [QDUUID currentUUID];
    
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    NSString *stringToHash = [NSString stringWithFormat:@"%@%@",udid,bundleIdentifier];
    NSString *uniqueIdentifier = [stringToHash stringFromMD5];
    return uniqueIdentifier;
}

+ (NSString *)uniqueDeviceIdentifier{
    NSString *uniqueIdentifier = [QDUUID newDeviceIdentifier];
    return uniqueIdentifier.length>0?uniqueIdentifier:@"";
}
@end
