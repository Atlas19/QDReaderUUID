//
//  QDUUID.h
//  QDReaderAtom
//
//  Created by YangYuxin on 16/7/13.
//  Copyright © 2016年 qidian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QDUUID : NSObject
+ (NSString *)creatUUID;
+ (NSString *)currentUUID;
+ (NSString *)uniqueDeviceIdentifier;
+ (NSString *)newDeviceIdentifier;
@end
