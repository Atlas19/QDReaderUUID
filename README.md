# QDReaderUUID

[![CI Status](http://img.shields.io/travis/Dylan Yang/QDReaderUUID.svg?style=flat)](https://travis-ci.org/Dylan Yang/QDReaderUUID)
[![Version](https://img.shields.io/cocoapods/v/QDReaderUUID.svg?style=flat)](http://cocoapods.org/pods/QDReaderUUID)
[![License](https://img.shields.io/cocoapods/l/QDReaderUUID.svg?style=flat)](http://cocoapods.org/pods/QDReaderUUID)
[![Platform](https://img.shields.io/cocoapods/p/QDReaderUUID.svg?style=flat)](http://cocoapods.org/pods/QDReaderUUID)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

QDReaderUUID is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "QDReaderUUID"
```

## Author

Dylan Yang, yangyuxin@yuewen.com

## License

QDReaderUUID is available under the MIT license. See the LICENSE file for more info.
