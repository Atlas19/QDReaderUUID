//
//  QDAppDelegate.h
//  QDReaderUUID
//
//  Created by Dylan Yang on 08/03/2016.
//  Copyright (c) 2016 Dylan Yang. All rights reserved.
//

@import UIKit;

@interface QDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
