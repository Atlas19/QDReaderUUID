//
//  main.m
//  QDReaderUUID
//
//  Created by Dylan Yang on 08/03/2016.
//  Copyright (c) 2016 Dylan Yang. All rights reserved.
//

@import UIKit;
#import "QDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QDAppDelegate class]));
    }
}
